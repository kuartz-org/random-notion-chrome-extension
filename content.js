const openRandomElementInNewTab = () => {
  const tabUrl = location.href;

  chrome.runtime.sendMessage({
    "message": "open_new_tab",
    "tabUrl": tabUrl,
  });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_browser_action" ) {
      openRandomElementInNewTab();
    }
  }
);

window.addEventListener ("load", myMain, false);

function myMain (evt) {
  var jsInitChecktimer = setInterval (checkForJS_Finish, 111);

  function checkForJS_Finish () {
    if ( document.querySelector('.notion-collection-view-item-add') ) {
      clearInterval (jsInitChecktimer);
      const notionTopbar = document.querySelector('.notion-collection-view-item-add');
      notionTopbar.insertAdjacentHTML("afterend", "<div class='btn-random-notion'>Random</div>");

      document.querySelector('.btn-random-notion').addEventListener('click', () => {
        openRandomElementInNewTab();
      })
    }
  }
}
