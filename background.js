const open_random_notion_tab = (token, url) => {
  fetch(`https://random-notion.herokuapp.com/?token=${token}&url=${url}`)
  .then(response => response.json())
  .then((data) => {
    chrome.tabs.create({"url": data.rsp.url});
  });
}

chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    const activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_browser_action"});
  });
});

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if(request.message === "open_new_tab") {
      chrome.cookies.get({ url: "https://www.notion.so", name: "token_v2" },
      function (cookie) {
        if (cookie) {
          const token = cookie.value;
          const url   = request.tabUrl;

          open_random_notion_tab(token, url);
        }
        else {
          console.log("Can't get cookie! Check the name!");
        }
      });

    }
  }
);
